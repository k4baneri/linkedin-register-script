# Linkedin Fake Account register script


This script is used to for fake accounts creation - it is Python script that use Selenium and Faker to generate fake data, proceed to Linkedin register page and fillout the form.


# Features

  - Generate Fake data such as : First Name, Last name, Email
  - Fill out the Linkedin register form with Fake data
  - Write down the the generated data into CSV file


# Limitations
 So basically, the mobile no. provider - PingMe made an update and now we can't automate the process of getting mobile no. from their website because the DOM elements now are having Dynamic classes - so we are not able to find the elements via xPath or CSS Selector and there is no other way to accomplish it with Python. 
 
 Also the other problem is that mobile no. provider Ping Me - they are forcing users to login with Email/Mobile no + OTP Code so we are not able to automate it. The solution is to keep browser open up with logged in account. 
 
 We are also limited to use the VPN from UK's because in this case LinkedIn forces us for mobile no. verificaation within UK's mobile no. and we got working mobile no. from PingMe for UK
 
I have been using VPN from Bugfinders(it's a crowdtesting platform) and we could go with this for our needs

Credentials:

k4baneri / SunShine99%

To use it we have to install OpenVPN, download certifikates from this repo and go through this quick tutorial
https://www.ovpn.com/en/guides/windows-openvpn-gui
 
# How to run?
 
First of all, we launch OpenVPN and connect to it (config files are attached to repository) and then run the script for 1st time 

To run the script:

You have to install [Python 3.8.6](https://www.python.org/downloads/release/python-386/) and any IDE 
([PyCharm](https://www.jetbrains.com/pycharm/download/) is really good) and then install packages, drivers:
 [Chromedriver](https://chromedriver.chromium.org/downloads)
 [Geckodriver](https://github.com/mozilla/geckodriver/releases)
 
 And put the drivers into your catalog:
 C:\Windows
 
 before you run the script with IDE by clicking "Run" Icon or SHIFT+10 we have to also install the packages (requirements.txt) and clone the repository
 

```
$ git clone https://bitbucket.org/k4baneri/linkedin-register-script.git
$ pip install -r > requirements.txt
$ main.py
```

Now we can run script. With the 1st run we have to login into PingMe. To do that process following:
1. Wait until script open the Google Chrome > PingMe website
2. Enter email testsrcloop001@gmail.com and reCaptcha
3. Go to http://gmail.com/ and login with following credentials
 - testsrcloop001@gmail.com 
 - srcLoop909@
4. Get OTP Code and enter onto PingMe website
5. That's it, you are logged in!

And from now we are able to run the script whenever we need create a fake account

!!! IMPORTANT NOTE !!!
After you logged into PingMe account remember to close the Browser (Google Chrome and Firefox) otherwise you may expect that script will fail

### Packages

Packages used in script

| Plugin | README |
| ------ | ------ |
| Faker | https://github.com/joke2k/faker|
| Selenium | https://www.selenium.dev/ |
| PyGetWindow | https://github.com/asweigart/PyGetWindow |


### Todos

 - Make script works a little bit faster
 - Store mobile numbers

License
----

MIT

