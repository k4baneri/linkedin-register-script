from selenium import webdriver
from selenium.webdriver.common.by import By
from faker import Faker
import time
import csv
from csv import DictWriter
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import pygetwindow as gw


""" Variables """
fake = Faker()
driver = webdriver.Firefox()
email_domain = "@protonmail.com"
first_name = fake.first_name()
last_name = fake.last_name()
full_name = first_name + " " + last_name
random_no = fake.random_int(0, 100000)
email = first_name + str(random_no) + email_domain
password = "SomeRandomPassword99%"
mydict =[{'Full name': full_name, 'Email address': email, 'Password': password}]
fields = ['Full name', 'Email address', 'Password']
filename = "file.csv"
# Setup wait for later
wait = WebDriverWait(driver, 10)
original_window = driver.current_window_handle


# selenium fill outs the register form
def fill_out_register_form():
    driver.get('https://www.linkedin.com/signup/cold-join?trk=guest_homepage-basic_nav-header-join')
    time.sleep(3)
    driver.find_element_by_id("email-address").send_keys(email)
    time.sleep(3)
    driver.find_element_by_id("password").send_keys(password)
    time.sleep(3)
    driver.find_element_by_class_name("join-form__form-body-submit-button").click()
    time.sleep(3)
    driver.find_element_by_id("first-name").send_keys(first_name)
    driver.find_element_by_id("last-name").send_keys(last_name)
    driver.find_element_by_id("join-form-submit").click()


# writing to csv file
def write_data_into_csv():
    with open(filename, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=fields)
        # Add dictionary as wor in the csv
        dict_writer.writerows(mydict)

# get mobile no from PingMe
def get_mobile_no():
    # setup
    chrome_options = Options()
    chrome_options.add_argument("user-data-dir=selenium")
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)

    # get PingMe website
    driver.get('https://messages.pingme.tel/')
    time.sleep(5)
    driver.find_element_by_class_name("mdi-shield-check").click()
    time.sleep(5)

    # select Linkedin
    menu = driver.find_element_by_xpath('//*[@id="new-code"]/div[1]/div[2]/div[1]/div[2]/div/div/div[1]/div[1]/div[1]')
    menu.click()
    time.sleep(5)
    actions = ActionChains(driver)
    actions.send_keys('lin')
    actions.perform()
    time.sleep(3)
    linkedin = driver.find_element_by_xpath("//*[@class='v-list-item__title' and contains(text(),'LinkedIn')]").click()
    time.sleep(3)

    # select UK
    driver.find_element_by_xpath('//*[@id="new-code"]/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[1]').click()
    time.sleep(3)
    scroll_down = driver.find_element_by_xpath("//*[contains(text(), 'Spain')]")
    actions = ActionChains(driver)
    actions.move_to_element(scroll_down).perform()
    time.sleep(3)
    driver.find_element_by_xpath("//*[contains(text(), 'United Kingdom')]").click()
    time.sleep(3)

    # Click Next button
    driver.find_element_by_xpath('//*[@id="new-code"]/div[1]/div[5]/button').click()
    time.sleep(5)

    # Get mobile no
    mobile_no = driver.find_element_by_xpath('//*[@id="new-code"]/div[1]/div[3]/div[2]/div[1]/div[1]').text

    # Remove country code from mobile_no
    global mobile_no_transformed
    mobile_no_transformed = mobile_no.replace('+44 ', '')

    chromeWindow = gw.getWindowsWithTitle('WebChat - Google Chrome')[0]
    chromeWindow.minimize()

    return mobile_no_transformed


def switch_to_firefox():
    firefoxWindow = gw.getWindowsWithTitle('Sign Up | LinkedIn - Mozilla Firefox')[0]
    firefoxWindow.activate()
    gw.getActiveWindow()

def enter_mobile_no():
    # switch to iframe
    driver.switch_to.frame(driver.find_element_by_xpath('/html/body/div[2]/main/section/iframe'))

    # fillout mobile no and click submit button
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="register-verification-phone-number"]').send_keys(mobile_no_transformed)
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="register-verification-phone-number"]').send_keys(Keys.RETURN)
    time.sleep(30)
    firefoxWindow = gw.getWindowsWithTitle('Sign Up | LinkedIn - Mozilla Firefox')[0]
    #firefoxWindow.minimize()

    #close chrome
    chromeWindow = gw.getWindowsWithTitle('WebChat - Google Chrome')[0]
    chromeWindow.close()

    # exit iframe
    driver.switch_to.default_content()

def get_otp():
    # setup
    chrome_options = Options()
    chrome_options.add_argument("user-data-dir=selenium")
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)
    # wait for code
    time.sleep(10)
    # get PingMe website
    driver.get('https://messages.pingme.tel/')
    time.sleep(60)
    # get otp code
    driver.find_element_by_xpath(
        '//*[@id="app"]/div[1]/main/div/div[1]/section[3]/div[1]/aside/ul/li[1]/div').click()
    time.sleep(5)
    otp_code = driver.find_element_by_xpath(
        '/html/body/div/div[1]/main/div/div[1]/section[3]/div[1]/div/div/div[3]/div/ul/li[2]/div/span/div').text
    global otp_code_transformed
    otp_code_transformed = otp_code.replace('Your LinkedIn verification code is ', '')
    print(otp_code_transformed)

def enter_otp_to_register():
    firefoxWindow = gw.getWindowsWithTitle('Sign Up | LinkedIn - Mozilla Firefox')[0]
    firefoxWindow.minimize()
    firefoxWindow.maximize()
    gw.getActiveWindow()

    # switch to iframe
    driver.switch_to.frame(driver.find_element_by_xpath('/html/body/div[2]/main/section/iframe'))
    time.sleep(5)
    valid_otp = otp_code_transformed.replace('.', '')
    driver.find_element_by_xpath('//*[@id="input__phone_verification_pin"]').send_keys(valid_otp)
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="input__phone_verification_pin"]').send_keys(Keys.RETURN)

def main():
    fill_out_register_form()
    get_mobile_no()
    switch_to_firefox()
    enter_mobile_no()
    get_otp()
    enter_otp_to_register()
    write_data_into_csv()
    print("account registered B)")

main()